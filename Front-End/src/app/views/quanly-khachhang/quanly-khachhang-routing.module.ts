import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuanlyKhachhangComponent } from './quanly-khachhang.component';

const routes: Routes = [
  {
    path: '',
    component: QuanlyKhachhangComponent,
    data: {
      title: 'Quản lý nhân viên'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuanlyKhachhangRoutingModule {}
