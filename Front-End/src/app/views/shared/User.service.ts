import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {User} from '../models/user.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders,HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService{
    constructor(public http: Http){}
    getUsers(){
        return this.http.get('http://localhost/api/product/readUser.php');
    }
    deleteUsers(usr:User)
    {
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({headers:headers});
        return this.http.post('http://localhost/api/product/deleteUser.php',usr,options).pipe((map((res:Response)=>res.json())));
    }

}