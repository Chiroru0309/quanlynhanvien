import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { sanphamAdminComponent } from './sanphamAdmin.component';

const routes: Routes = [
  {
    path: '',
    component: sanphamAdminComponent,
    data: {
      title: 'Quản lý sản phẩm'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class sanphamAdminRoutingModule {}
