﻿## Required

``` bash
node >= 8.9.4
npm > 5.6.0
ng >= 6.1.3  
xampp >= 3.2.3

Back-End:
...
-  Vào trong thư mục Back-End: Copy thư mục api
-  Paste vào đường dẫn C:\xampp\htdoc\ 
-  Bật xampp chạy 2 server Apache và MySQL
-  Vào localhost/phpmyadmin rồi tạo 1 database có ngôn ngữ utf8/unicode
-  Copy câu lệnh sql trong thư mục databse rồi tạo các bảng vầ chèn dữ liệu.



Front-End :

```
## Installation

# di chuyển vào trong app's directory
$ cd Software-sharing-website
$ cd Front-End

# install app's dependencies
$ npm install
```

## Usage

``` bash
# serve with hot reload at localhost:4200.
$ npm start --o
# serve with hot reload at localhost:*.
$ npm start --o --port ***
# build for production with minification
$ ng build
```